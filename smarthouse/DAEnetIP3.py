#!/usr/bin/python
#coding=utf-8

import socket
import sys
import time
import netsnmp
from pprint import pprint
from common.driver import Device
#from common.RC4 import RC4

class DAEnetIP3(Device):
	'''Низкоуровневый драйвер для устройства DAEnetIP3.
	'''
	def send_data(self, function, data): # Отправка сообщения	
		try:
		    res = ''
		    message = '00' + function + '=' + data + ';'
		    
		    #message = "TXivxqORntP6uZuiHYA4aOn9"
		    #message = self.RC4.encrypt(message)

		    ##print ('sending "%s"' % message )
		    
		    self.sock.sendall(message)
		    data = self.sock.recv(64)
		    if data[2] == "E":
		    	print("Error data recieve : %s" % data)
		    	return -1
		    else:	
		    	##print ('received "%s"' % data)
		    	return data
		except:
			print ('Error sending data')

	def decode_mas(self): # 2 -> 16 Пока не используется, что меняло надо тестить
		res = ''
		for i in range(4):
			a = 0
			for j in range(4):	
				a += ( 1 - self.mas[i*4+j]*(2**j) )
			if (a<10):
				res += str(a)
			else:
				res += chr(ord('A') + (a-10))	
		res = res[::-1]	# разворот строки
		return res

	def encode_mas(self, s): # 16 -> 2 Обновляет массив состояний
		s = s[::-1] 
		kol = len(s)
		mas_temp = [0] * (kol*4)
		for i in range(kol):
			if (s[i]>='A'):
				a = ord(s[i]) - ord('A') + 10
			else:
				a = ord(s[i]) - ord('0')	
			for j in range(4):
				mas_temp[i*kol + j] = (a % 2)
				a /= 2		
		return mas_temp		

	def on_pin(self, num): # number 1-16
		if num < 1 or num > 16:
			print("Error number pin")
		else:	
			num = num - 1
			if (num>='A'):
				a = chr(num - 10 + ord('A'))
			else:
				a = str(num)
			res = self.send_data('AS'+a, '0')
			if res!=-1:
				self.mas[num] = 1

	def off_pin(self, num): # number 1-16
		if num < 1 or num > 16:
			print("Error number pin")
		else:	
			num = num - 1
			if (num>='A'):
				a = chr(num - 10 + ord('A'))
			else:
				a = str(num)
			res = self.send_data('AS'+a, '1')
			if res!=-1:
				self.mas[num] = 0

	def pin_sos(self):
		res = self.send_data('ASG', '?')
		if res != -1:
			return res[6:][:4] # Вытаскиваем только состояние
		else:
			return -1		

	def __init__(self, *args, **kwargs):
		#super(DAEnetIP3, self).__init__(*args, **kwargs)
		#self.host = self.db_dev.ext.ip
		#self.community = self.db_dev.ext.community
		print "INIT"
		#self.RC4 = RC4('admin')
		self.mas = [0] * 16 # Состояние output ламп 0 - OF, 1 - ON
		self.mas_input = [0] * 8  # Состояние input ламп  0 - OFF, 1 - ON
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server_address = ('192.168.0.100', 1010)
		self.sock.connect(self.server_address)
		self.refresh()

	def refresh(self): #TODO: Добавить к CRON проверку состояния
		sost = self.pin_sos()
		if sost!=-1:
			self.mas = [ 1-i for i in self.encode_mas(sost)]
		
	def get_pin_state(self, pin): #Теперь все кэшируется
		return self.mas[pin-1]

	def set_pin_state(self, pin, state):
		if state==1:
			self.on_pin(pin)
		else:
			self.off_pin(pin)
		return 0

	def get_input_state(self, pin):
		res = d.send_data("BV"+str(pin), "?")		
		if res!=-1:
			return int(res[6:][0])

if __name__ == '__main__':
	d = DAEnetIP3()
	
	#print (d.mas)
	#d.set_pin_state(1, 1)
	#print (d.mas)
	sos = 0
	while (1):
		time.sleep(0.02)
		sos_new = d.get_input_state(2)
		if sos != sos_new:
			print ("Change %d", sos_new);
		sos = sos_new	

	#d.send_data("BV1", "?")
	#d.send_data("BV2", "1")
	
	#print (d.get_input_state(1))
	#print (d.get_input_state(2))

	d.sock.close()