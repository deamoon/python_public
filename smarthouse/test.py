#!/usr/bin/python
#coding=utf-8

import socket
import sys

mas = [1] * 16

def decode_mas():
	res = ''
	for i in range(4):
		a = 0
		for j in range(4):	
			a += mas[i*4+j]*(2**j)
		if (a<10):
			res += str(a)
		else:
			res += chr(ord('A') + (a-10))	
	res = res[::-1]	# разворот строки
	return res

def encode_mas(s):
	s = s[::-1]
	for i in range(4):
		if (s[i]>='A'):
			a = ord(s[i]) - ord('A') + 10
		else:
			a = ord(s[i]) - ord('0')	
		for k in range(4):
			mas[i*4 + k] = a % 2
			a /= 2		
	
#print decode_mas()
data = '2231'
print data
encode_mas(data)
for i in range(16):
	print (mas[i]),



