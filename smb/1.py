#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

def isFilm(film):
	return film.endswith((".mkv", ".avi"))

def correctStr(s):
	if s.startswith("'") and s.endswith("'"):
		s = "'%s'" % s[1:-1].decode('string_escape')
	elif s.startswith("u'") and s.endswith("'"):
		s = "u'%s'" % s[2:-1].decode('unicode_escape').encode('utf8')
	return s	

f = open("movie_list.txt", 'w')
# f.write("INSERT INTO `gimna1_studentplus`.`kinomipt` (`id`, `name`, `rnamerus`, `rnameen`, `tag`) VALUES ")
films = set([])

for root, dirs, files in os.walk("/home/deamoon/.gvfs/video on video.campus/movie/!title/"):
	for s in files:
		s = correctStr(s)
		root = correctStr(root)
		if isFilm(s):
			if not s in films:
				name = root[41:].split('/')[4]
				films.add(s)
				s = "%s###%s###%s###%d" % (s, name, root[41:], os.path.getsize(root+'/'+s))
				f.write(s+'\n')


