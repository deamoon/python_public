import re, sys

def main():
	pattern = sys.argv[1]
	input = sys.argv[2]

	# pattern = ".*a"
	# input = "werf"

	print(bool(re.match(pattern, input)))

if __name__ == '__main__':
	main()