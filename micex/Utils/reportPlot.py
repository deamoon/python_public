#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""pihta_fond monitoring reports"""

import pylab
import datetime
from matplotlib.pyplot import figure, show
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from numpy import arange

def main():

	dates = []
	precents = []
	name = "pihta_fond.log.daily"
	#name = "pihta_fond.log.monthly"
	daily = name.rsplit('.', 1)[1] in ["daily"]

	with file (name, "r") as fp:
  		for line in fp:
  			try:
				year, _, _, precent = line.split()
				year = year.split('-')
				if daily:
					date = datetime.datetime(int(year[0]), int(year[1]), int(year[2]))
				else:
					date = datetime.datetime(int(year[0]), int(year[1]), 1)	
				dates.append(date)
				precents.append(precent)
  			except:
  				pass	
  	
	fig = figure()
	ax = fig.add_subplot(111)

	ax.plot(dates, precents)

	ax.set_xlim(dates[0], dates[-1])
	ax.set_ylim(0, 120)


	ax.xaxis.set_major_locator( DayLocator(interval=10) )
	ax.xaxis.set_minor_locator( HourLocator(arange(0,25,6)) )

	if daily:
		ax.xaxis.set_major_formatter( DateFormatter('%Y-%m-%d') )
	else:
		ax.xaxis.set_major_formatter( DateFormatter('%Y-%m') )	

	ax.fmt_xdata = DateFormatter('%Y-%m-%d %H:%M:%S')

	fig.autofmt_xdate()

	pylab.xlabel("Day of measurement of availability")
	pylab.ylabel("Availability percent")
	
	fig.savefig(name + '.png')
	#show()

if __name__ == '__main__':
	main()	