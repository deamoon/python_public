#!/usr/bin/python
#coding=utf-8

import sys, os
import argparse

def csv2Excel(file_names, res_name, work_dir):
	fileList = [str(f) for f in file_names]
	csvFileList = [f for f in fileList if f[::-1][:3][::-1] in ("csv")]
	if not csvFileList:
		return
	res_file = open(os.path.join(work_dir, res_name), "w")	
	rownum = 0
	for f in csvFileList:
		name = os.path.split(f)[1]
		res_file.write("packet;%s;%s" % (os.path.splitext(name)[0].upper(), "\n"))
		name = name.upper()
		if name.startswith('ORDER'):
			type_tr = "ORDER"
		elif name.startswith('RCORDER'):
			type_tr = "CCP_REPO_ORDER"
		elif name.startswith('NEGDEAL'):
			type_tr = "NEGDEAL"
		elif name.startswith('RNEGDEAL'):
			type_tr = "EXT_REPO_NEGDEAL"
		elif name.startswith('RCNEGDEAL'):
			type_tr = "CCP_REPO_NEGDEAL"
		elif name.startswith('PRIMARY'):
			type_tr = "EXT_TRADE"
		elif name.startswith('TRANSFER'):
			type_tr = "SECTRANSFER"
		else:
			type_tr = "UNKNOWN"
			print("Error name file %s" % name)								
		res_file.write(type_tr)				

		with open(f, 'r') as f:
			for row in f.readlines():
				kol = row.count(';') + 1
				res_file.write(';'+row)		
			res_file.write(';' * kol + "\n")		

def main():
	parser = argparse.ArgumentParser(description="Program unions csv files, "\
													"name must start with "\
													"ORDER, RCORDER, NEGDEAL, RNEGDEAL, RCNEGDEAL, PRIMARY, TRANSFER")
	parser.add_argument('-qt', '-Qt', help='flag run pyQt version', action="store_true")
	parser.add_argument("-d", help='dir with csv files (default = \'.\')', default = '.')
	parser.add_argument("-n", help='name result file (default = \'test_data.csv\')', default = 'test_data.csv')
	args = parser.parse_args()
	args.qt = 1
	if args.qt:
		from PyQt4 import Qt, QtCore, QtGui        
		app = Qt.QApplication(sys.argv)
		window = Qt.QWidget()
		le = QtGui.QLineEdit(window)

		def choose_file():
			file_names = QtGui.QFileDialog.getOpenFileNames(window, "Open Data File", ".", "CSV data files (*.csv)")
			if file_names:
				work_dir = os.path.split(str(file_names[0]))[0]
				csv2Excel(file_names, str(le.text()), work_dir)

		layout2 = Qt.QVBoxLayout()
		layout2.setContentsMargins(3, 3, 3, 3)
		layout2.setSpacing(3)
		le.setText("test_data.csv")
		button1 = Qt.QPushButton(u"Выберите файлы")
		QtCore.QObject.connect(button1, QtCore.SIGNAL("clicked()"), choose_file)
		layout2.addWidget(le)
		layout2.addWidget(button1)
		window.setLayout(layout2)
		window.show()
		app.exec_()
	else:
		res_name = args.n
		file_names = [os.path.join(args.d, f) for f in os.listdir(args.d)]
		csv2Excel(file_names, res_name, args.d)

if __name__ == "__main__" :
	main()

