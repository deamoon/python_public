#!/usr/bin/python2.7
#coding=utf-8

class Formula():
	class Sym:
		_Num, _Plus, _Mul, _Open, _Close, _End, _Minus, _Del = range(8)
	
	class ErrorFormula(Exception):
	    def __init__(self, value):
	        self.value = value
	    def __str__(self):
	        return repr(self.value)
	
	def __init__(self, s):
		self.formula = s
		self.curlex = -1
		self.it, self.vl = 0, 0

	def nextlexem(self):
		if self.it >= len(self.formula):
			self.curlex = self.Sym._End
			return
		c = self.formula[self.it]
		if c == '(':
			self.curlex = self.Sym._Open
			self.it = self.it + 1	
		elif c == ')':
			self.curlex = self.Sym._Close	
			self.it = self.it + 1	
		elif c == '+':
			self.curlex = self.Sym._Plus
			self.it = self.it + 1	
		elif c == '-':
			if self.it == 0 or not self.formula[self.it - 1].isdigit():
				self.curlex = self.Sym._Minus
				self.it = self.it + 1		
		elif c == '*':
			self.curlex = self.Sym._Mul
			self.it = self.it + 1	
		elif c.isdigit():
			self.curlex = self.Sym._Num
			self.vl = 0
			while self.it < len(self.formula) and self.formula[self.it].isdigit():			
				self.vl = self.vl * 10 + int(self.formula[self.it])
				self.it = self.it + 1
		else:
			raise self.ErrorFormula(u"unknown symbol " + str(self.it))
	
	def expr(self):
		a = self.item()
		while self.curlex == self.Sym._Plus or self.curlex == self.Sym._Minus:
			c = self.curlex
			self.nextlexem()
			b = self.item()
			if c == self.Sym._Plus:
				a = a + b
			else:
				a = a - b	
		return a	
	
	def item(self):
		a = self.mult()
		while self.curlex == self.Sym._Mul:
			self.nextlexem()
			a = a * self.mult()
		return a
		
	def mult(self):
		if self.curlex == self.Sym._Num:
			mult_res = self.vl
			self.nextlexem()
		elif self.curlex == self.Sym._Open:
			self.nextlexem()
			mult_res = self.expr()
			if self.curlex == self.Sym._Close:
				self.nextlexem()
			else:
				raise self.ErrorFormula(u"brackets " + str(self.it))
		else:
			raise self.ErrorFormula(u"problem " + str(self.it))
		return mult_res	
	
import floatcalc as calc

if __name__ == '__main__':
	try:
		a = Formula("1-5")
		a.nextlexem()
		v = a.expr()
		if a.curlex != a.Sym._End:
			raise a.ErrorFormula(u"end " + a.it)
		print (v)
	except a.ErrorFormula as er:
		print("ErrorFormula : %s" % er)		

