#!/usr/bin/python
#coding=utf-8

import xlrd
import floatcalc as calc

class ParserError(Exception):
    def __init__(self, value):
	    self.value = value
    def __str__(self):
        return repr(self.value)
class ErrorSyntaxExcel(ParserError): pass

class ParseExcel:
	def empty(self, row):
		print row
		if type(row)!=type("") and type(row)!=type([]):
			return 0
		if row == "" or row[0] == '#': # Если row строка
			return 1
		q = 1
		for c_el in row: # Если row список строк
			if c_el == "":
				continue
			if str(c_el)[0] != '#':
				q = 0
				break
		return q	

	def __init__(self, file, page):
	# TODO Функции, процедуры
		self.file = file
		self.page = page
		rb = xlrd.open_workbook(self.file, formatting_info=True)
		self.sheet = rb.sheet_by_index(self.page)

		self.name, key = [], []
		key_dict , self.ordersData, var_dict = {}, {}, {}
		start, kol = 0 , 0

		for rownum in range(self.sheet.nrows):
			row = self.sheet.row_values(rownum)
			if self.empty(row): # Пустая строка
				start = 0
			elif start == 0: # Стартовое состояние
				if row[0] == "VAR":
					start = 2
				elif row[0] == "ORDER": # Читаем Header 
					# TODO Где хранить информацию, что я ORDER
					start = 1
					key = row[1:] # Список ключей без ORDER
				else:
					raise ErrorSyntaxExcel(row[0])
			elif start == 1: # Читаем List Values
				if self.empty(row[0]): # Определяем имя строки
					name = "_test_" + str(kol)
					kol = kol + 1	
					isName = 0 # Флаг именнованности строки			
				else:
					name = row[0]		
					isName = 1
				for colnum in range(len(key)):
					if self.empty(row[colnum+1]):
						key_dict[key[colnum]] = ""	
					else:
						key_dict[key[colnum]] = calc.formula(row[colnum+1], var_dict) # Вычисляем значение ячейки с учетом переменных (colnum+1 сместилась нумерация из-за первого столбца)
						if isName:
							var_dict[name+':'+key[colnum]] = key_dict[key[colnum]]
				self.ordersData[name] = key_dict	
				key_dict = {}
			elif start == 2: # Читаем VAR
				var_dict[row[0]] = row[1]		
			else:
				raise ParserError("Error in \'start\' value")
	
	def write_xls(self):
		for rownum in range(self.sheet.nrows):
			row = self.sheet.row_values(rownum)
			for c_el in row:
				if c_el == '':
					print ";",
				else:
					print c_el,	
			print

	def write_res(self):
		print self.ordersData.keys()		
		print ("------------------")
		print self.ordersData.values()		
		print ("------------------")
		print (self.ordersData)
		
if __name__ == '__main__':
	a = ParseExcel("3.xls", 0)
	a.write_res()


