#!/usr/bin/python2.7
#coding=utf-8

from __future__ import division

# Uncomment the line below for readline support on interactive terminal
# import readline  
import re
from pyparsing import Word, alphas, ParseException, Literal, CaselessLiteral \
, Combine, Optional, nums, Or, Forward, ZeroOrMore, StringEnd, alphanums
import math


class CalcError(Exception):
    def __init__(self, value):
      self.value = value
    def __str__(self):
        return repr(self.value)

exprStack = []

def pushFirst( str, loc, toks ):
    exprStack.append( toks[0] )

# define grammar
point = Literal('.')
e = CaselessLiteral('E')
plusorminus = Literal('+') | Literal('-')
number = Word(nums) 
integer = Combine( Optional(plusorminus) + number )
floatnumber = Combine( integer +
                       Optional( point + Optional(number) ) +
                       Optional( e + integer )
                     )

ident = Combine( Optional(plusorminus) + Word(alphas,alphanums + '_' + ':') )
# TODO plusorminus | '!'


plus  = Literal( "+" )
minus = Literal( "-" )
mult  = Literal( "*" )
div   = Literal( "/" )
lpar  = Literal( "(" ).suppress()
rpar  = Literal( ")" ).suppress()
addop  = plus | minus
multop = mult | div
expop = Literal( "^" )
assign = Literal( "=" )

expr = Forward()
atom = ( ( e | floatnumber | integer | ident ).setParseAction(pushFirst) | 
         ( lpar + expr.suppress() + rpar )
       )
        
factor = Forward()
factor << atom + ZeroOrMore( ( expop + factor ).setParseAction( pushFirst ) )
        
term = factor + ZeroOrMore( ( multop + factor ).setParseAction( pushFirst ) )
expr << term + ZeroOrMore( ( addop + term ).setParseAction( pushFirst ) )
bnf = Optional((ident + assign)) + expr

pattern =  bnf + StringEnd()

# map operator symbols to corresponding arithmetic operations
opn = { "+" : ( lambda a,b: a + b ),
        "-" : ( lambda a,b: a - b ),
        "*" : ( lambda a,b: a * b ),
        "/" : ( lambda a,b: a / b ),
        "^" : ( lambda a,b: a ** b ) }

#TODO
#procedures = { "!TIME" : time.clock()
#             }

# Recursive function that evaluates the stack
def evaluateStack(variables, s):
  op = s.pop()
  if op in "+-*/^":
    op2 = evaluateStack( variables, s )
    op1 = evaluateStack( variables, s )
    return opn[op]( op1, op2 )
  elif re.search('^[-+]?[a-zA-Z][a-zA-Z0-9_]*[:]?[a-zA-Z0-9_]*$',op): # TODO [!]?
    if op[0] == '+':
      return variables[op[1:]]  
    elif  op[0] == '-':  
      return (-1) * variables[op[1:]]    
    #elif op[0] == '!' or (len(op)>1 and op[1] == '!'): TODO, не забудь про минусы, они все портят
      ###
    elif variables.has_key(op):
      return variables[op]
    else:
      raise CalcError("Unknown variable " + str(op)) # Нет переменной
  elif re.search('^[-+]?[0-9]+$',op):
    return long( op )
  else:
    return float( op )

def formula(input_string, variables):    
  global exprStack
  #variables = {}
  input_string = str(input_string)
  try:
    L=pattern.parseString( input_string )
  except ParseException,err:
    L=['Parse Failure',input_string]
  if len(L)==0 or L[0] != 'Parse Failure':
    result=evaluateStack(variables, exprStack)
    return result
  else:
    raise CalcError("Parse Failure\n" + 
                    err.line + "\n" + 
                    " "*(err.column-1) + "^" + "\n" + 
                    err + "\n")

if __name__ == '__main__':
    print formula("y-x", {'x':1, 'y':2})      



