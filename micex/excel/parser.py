#!/usr/bin/python
#coding=utf-8

import xlrd

class ParseExcel:
	def empty(self, row):
		q = 1
		for c_el in row:
			if c_el != "" and c_el[0] != '#':
				q = 0
				break
		return q	

	def __init__(self, file, page):
	# TODO Функции, ссылки, арифметика	
		self.file = file
		self.page = page
		rb = xlrd.open_workbook(self.file, formatting_info=True)
		self.sheet = rb.sheet_by_index(self.page)

		self.name, key = [], []
		key_dict , self.ordersData, per_dict = {}, {}, {}
		start, kol = 0 , 0

		for rownum in range(self.sheet.nrows):
			row = self.sheet.row_values(rownum)
			if self.empty(row): # Пустая строка
				start = 0
			elif start == 0: # Стартовое состояние
				if row[0] == "VAR":
					start = 2
				elif row[0] == "ORDER": # Читаем Header 
					# TODO Где хранить информацию, что я ORDER
					start = 1
					key = row[1:]
				else:
					print("Error syntax Excel file: %s " % row[0])				
			elif start == 1: # Читаем List Values
			# TODO У строки теперь есть имя, первым полем
				for colnum in range(len(row)):
					if key[colnum] != "" and c_el[0] != '#':
						if row[colnum] in per_dict:
							key_dict[key[colnum]] = per_dict[row[colnum]]
						else:
							key_dict[key[colnum]] = row[colnum]	
					else:
						key_dict[key[colnum]] = ''		
				self.ordersData["Test_" + str(kol)] = key_dict	
				key_dict = {}
				kol = kol + 1					
			elif start == 2: # Читаем VAR
				per_dict[row[0]] = row[1]		
			else:
				print("Error syntax Excel file")		
	
	def write(self):
		for rownum in range(self.sheet.nrows):
			row = self.sheet.row_values(rownum)
			#print rownum
			#print type(row)
			for c_el in row:
				
				if c_el == '':
					print ";",
				else:
					print c_el,	
			print
		
if __name__ == '__main__':
	a = ParseExcel("1.xls", 2)
	#a.write()
	print a.ordersData


