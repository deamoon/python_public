#!/usr/bin/python2.7
#coding=utf-8

import pywapi
import string
import pprint
import urllib2

class WeatherException(Exception):
	pass

class InetWeather():
	def __init__(self, city):
		self.weather = {}
		self.info = {}
		self.city = city
	def is_service_on(self):
	    raise NotImplementedError("should have implemented this on")		
	def _is_url_on(self, url):
	    try:
	        response = urllib2.urlopen(url,timeout=1)
	        return True
	    except urllib2.URLError:
	    	pass
	    return False	    

class InetWeather_Weather(InetWeather):
	def __init__(self, city):
		InetWeather.__init__(self, city)
		city_id = pywapi.get_location_ids(self.city)
		if "error" in city_id:
			raise WeatherException("Problem with connect service")
		if not city_id:
			raise WeatherException("Can't find city")	
		if len(city_id.keys()) > 1:
			raise WeatherException("Find a lot of condidates for city")	
		self.id = city_id.keys()[0]
		self._poll()

	def is_service_on(self):
		return self._is_url_on('http://xml.weather.com')

	def _poll(self):
		weather_res = pywapi.get_weather_from_weather_com(self.id)
		if "error" in weather_res:
			raise WeatherException("Problem with connect service")

		w = []
		w.append({})
		w[0]["sunrise"] = weather_res["forecasts"][0]["sunrise"]
		w[0]["sunset"] = weather_res["forecasts"][0]["sunset"]
		w[0]["pressure"] = weather_res["current_conditions"]["barometer"]["reading"]
		w[0]["temp"] = weather_res["current_conditions"]["temperature"]
		w[0]["wind_speed"] = weather_res["current_conditions"]["wind"]["speed"]
		w[0]["date"] = weather_res["forecasts"][0]["date"]
		w[0]["text"] = weather_res["current_conditions"]["text"]
		w[0]["humidity"] = weather_res["current_conditions"]["humidity"]

		inf = {}
		inf["geo_lat"] = weather_res["location"]["lat"]
		inf["geo_long"] = weather_res["location"]["lon"]
		inf["city"] = weather_res["location"]["name"]
		inf["temp_symbol"] = weather_res["units"]["temperature"]
		inf["speed_symbol"] = weather_res["units"]["speed"]
		inf["pressure_symbol"] = weather_res["units"]["pressure"]
		inf["distance_symbol"] = weather_res["units"]["distance"]

		i = 0
		for t in weather_res["forecasts"]:
			i += 1
			w.append({})
			w[i]["date"] = t["date"]
			w[i]["high"] = t["high"]
			w[i]["low"] = t["low"]

		self.weather = w
		self.info = inf	
	
class InetWeather_Yahoo(InetWeather):
	def __init__(self, city):
		InetWeather.__init__(self, city)
		city_id = pywapi.get_location_ids(self.city)
		if "error" in city_id:
			raise WeatherException("Problem with connect service")
		if not city_id:
			raise WeatherException("Can't find city")	
		if len(city_id.keys()) > 1:
			raise WeatherException("Find a lot of condidates for city")	
		self.id = city_id.keys()[0]
		self._poll()

	def is_service_on(self):
		return self._is_url_on('http://xml.weather.yahoo.com')

	def _poll(self):	
		yahoo_res = pywapi.get_weather_from_yahoo(self.id)
		if "error" in yahoo_res:
			raise WeatherException("Problem with connect service")

		w = []
		w.append({})
		w[0]["sunrise"] = yahoo_res["astronomy"]["sunrise"]
		w[0]["sunset"] = yahoo_res["astronomy"]["sunset"]
		w[0]["pressure"] = yahoo_res["atmosphere"]["pressure"]
		w[0]["temp"] = yahoo_res["condition"]["temp"]
		w[0]["wind_speed"] = yahoo_res["wind"]["speed"]
		w[0]["date"] = yahoo_res["condition"]["date"]
		w[0]["text"] = yahoo_res["condition"]["text"]
		w[0]["humidity"] = yahoo_res["atmosphere"]["humidity"]

		inf = {}
		inf["geo_lat"] = yahoo_res["geo"]["lat"]
		inf["geo_long"] = yahoo_res["geo"]["long"]
		inf["city"] = yahoo_res["location"]["city"]
		inf["temp_symbol"] = yahoo_res["units"]["temperature"]
		inf["speed_symbol"] = yahoo_res["units"]["speed"]
		inf["pressure_symbol"] = yahoo_res["units"]["pressure"]
		inf["distance_symbol"] = yahoo_res["units"]["distance"]

		i = 0
		for t in yahoo_res["forecasts"]:
			i += 1
			w.append({})
			w[i]["date"] = t["date"]
			w[i]["high"] = t["high"]
			w[i]["low"] = t["low"]

		self.weather = w
		self.info = inf


def main():
	ya = InetWeather_Yahoo("Moscow, Russia")
	#print ya.weather
	#print ya.info
	w = InetWeather_Weather("Moscow, Russia")
	#print w.weather
	#print w.info

if __name__ == '__main__':
	main()