import urllib2, base64
opener = urllib2.build_opener(urllib2.HTTPHandler)

data2 = ""
f = open('test.tar.gz', 'rb')
length = 0
while True: 
   bytes = f.read(2048)
   data2 += bytes;
   length += len(bytes)
   if not bytes: break   
f.close()

request = urllib2.Request("https://webdav.yandex.ru/test/test.tar.gz", data=data2)
base64string = base64.encodestring('%s:%s' % ("xxxx@yandex.ru", "xxxxx")).replace('\n', '')
request.add_header("Authorization", "Basic %s" % base64string) 
request.add_header("Content-Length", "%d" % length)  
request.get_method = lambda: 'PUT'
result = urllib2.urlopen(request)

