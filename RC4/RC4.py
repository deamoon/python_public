#!/usr/bin/python
#coding=utf8
 
import base64
import sys
 
class RC4:
    def __init__(self, key):
        self.key = key

    def rc4_crypt(self, data):
        S = range(256)
        j = 0
        out = []
        for i in range(256):
            j = (j + S[i] + ord( self.key[i % len(self.key)] )) % 256
            S[i] , S[j] = S[j] , S[i]
        for char in data:
            i = j = 0
            i = ( i + 1 ) % 256
            j = ( j + S[i] ) % 256
            S[i] , S[j] = S[j] , S[i]
            out.append(chr(ord(char) ^ S[(S[i] + S[j]) % 256])) 
        return ''.join(out)
     
    def encrypt(self, data, encode = base64.b64encode ):
        data = self.rc4_crypt(data)
        if encode:
            data = encode(data)
        return data

    def decrypt(self, data, decode = base64.b64decode ): 
        if decode:
            data = decode(data)
        return self.rc4_crypt(data)
     
'''     
if __name__ == '__main__':
    a = RC4('admin')
    data = '00ASG=0000'
    print (a.encrypt(data))
    print (a.decrypt(a.encrypt(data)))
'''